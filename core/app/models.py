from core.extensions import db


class Category(db.Model):
    __tablename__ = 'cat_categorias'
    __table_args__ = {'implicit_returning': False}
    idCategoria = db.Column(db.Integer, primary_key=True, nullable=False)
    icon = db.Column(db.String(100))
    image = db.Column(db.String(100))
    name = db.Column(db.String(100))
    title_list = db.Column(db.String(100))
    url = db.Column(db.String(100))
    view  = db.Column(db.Integer)
    state = db.Column(db.String(100))

    def __init__(self, icon, image, name, title_list, url, view, state):
        self.icon = icon
        self.image = image
        self.name = name
        self.title_list = title_list
        self.url = url
        self.view = view
        self.state = state


class Employee(db.Model):
    __tablename__ = 'cat_empleados'
    __table_args__ = {'implicit_returning': False}
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    firstName = db.Column(db.String(100))
    lastName = db.Column(db.String(100))
    position = db.Column(db.String(100))
    startDate = db.Column(db.Date())
    salary = db.Column(db.Float())
    username = db.Column(db.String(), nullable=True)
    password = db.Column(db.String(), nullable=True)

    def __init__(self, firstName, lastName, position,
                 startDate, salary, username=None, password=None):
        self.firstName = firstName
        self.lastName = lastName
        self.position = position
        self.startDate = startDate
        self.salary = salary
        self.username = username
        self.password = password

    def save_username_password(self,  username, password):
        self.username = username
        self.password = password
        db.session.commit()

    @classmethod
    def username_is_available(cls, username: str, employee_id: int) -> int:
        return db.session.query(func.count(cls.id)) \
            .filter(cls.username == username, cls.id != employee_id).scalar()
        
class InvoiceCustomer(db.Model):
    __tablename__ = 'cat_clientes_facturacion'
    __table_args__ = {'implicit_returning': False}
    id_cliente_facturacion = db.Column(db.Integer, primary_key=True, nullable=False)
    id_cliente  = db.Column(db.Integer)
    razon_social = db.Column(db.String(100))
    rfc = db.Column(db.String(100))
    calle = db.Column(db.String(100))
    numero_exterio = db.Column(db.String(100))
    numero_interior = db.Column(db.String(100))
    tipo_persona = db.Column(db.Integer)
    clave_pais = db.Column(db.String(100))
    estado = db.Column(db.String(100))
    ciudad = db.Column(db.String(100))
    colonia = db.Column(db.String(100))
    contacto = db.Column(db.String(100))
    email_facturacion = db.Column(db.String(100))

    def __init__(self, razon_social, rfc, calle, numero_exterio, numero_interior,
                tipo_persona, clave_pais, estado, ciudad, colonia, contacto,
                email_facturacion, id_cliente=0):
        self.razon_social = razon_social
        self.rfc = rfc
        self.calle = calle
        self.numero_exterio = numero_exterio
        self.numero_interior = numero_interior
        self.tipo_persona = tipo_persona
        self.clave_pais = clave_pais
        self.estado = estado
        self.ciudad = ciudad
        self.colonia = colonia
        self.contacto = contacto
        self.email_facturacion = email_facturacion
        self.id_cliente = id_cliente
    


'''
CREATE TABLE `marketplace`.`cat_clientes_facturacion` (
  `id_cliente_facturacion` INT NOT NULL,
  `id_cliente` INT NULL,
  `razon_social` VARCHAR(45) NULL,
  `rfc` VARCHAR(13) NULL,
  `calle` VARCHAR(45) NULL,
  `numero_exterio` VARCHAR(10) NULL,
  `numero_interior` VARCHAR(10) NULL,
  `tipo_persona` INT NULL,
  `clave_pais` VARCHAR(5) NULL,
  `estado` VARCHAR(15) NULL,
  `ciudad` VARCHAR(25) NULL,
  `colonia` VARCHAR(30) NULL,
  `contacto` VARCHAR(50) NULL,
  `email_facturacion` VARCHAR(50) NULL,
  `cat_clientes_facturacioncol` VARCHAR(45) NULL,
  PRIMARY KEY (`id_cliente_facturacion`));
'''