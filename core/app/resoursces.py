from email import message
from flask import Flask, request, jsonify
from flask_cors import CORS, cross_origin
from flask_restful import Api, Resource, request

from ..extensions import db
from .models import	Employee, Category, InvoiceCustomer
from .schemas import (
    employee_schema,
    employees_schema,
    category_schema,
    categories_schema,
    customer_schema,
    customers_schema
)
from .utils.estados import estados
from .utils.municipios import municipios_por_estado


class EmployeeListResource(Resource):
    support_cors = True
    cors_origin = '*'
    cors_headers = 'origin, content-type, x-request-with'

    @cross_origin()
    def get(self):
        employees = Employee.query.all()
        if not employees:
            return {'data': []}, 404
        return {'data': employees_schema.dump(employees)}, 200

    @cross_origin()
    def post(self):
        payload = {
            'firstName': request.json['firstName'],
            'lastName': request.json['lastName'],
            'position': request.json['position'],
            'startDate': request.json['startDate'],
            'salary': request.json['salary'],
            
        }
        startDate = request.json['startDate']
        new_employee = Employee(**payload)
        db.session.add(new_employee)
        db.session.commit()
        return {'data': employee_schema.dump(new_employee)}, 201


class EmployeeResource(Resource):
    support_cors = True
    cors_origin = '*'

    def get(self, id):
        business = Employee.query.get_or_404(id)
        return employee_schema.dump(business)

    @cross_origin()
    def put(self, id):
        employee = Employee.query.get_or_404(id)
        employee.firstName = request.json['firstName']
        employee.lastName = request.json['lastName']
        employee.position = request.json['position']
        employee.salary = request.json['salary']
        db.session.commit()
        return {'data': employee_schema.dump(employee)}, 200

    def business(self, station_id):
        business = Business.query.get_or_404(id)
        db.session.delete(business)
        db.session.commit()
        return '', 204

class EmployeeSavePassword(Resource):
    support_cors = True
    cors_origin = '*'

    def put(self, employee_id):
        employee = Employee.query.get_or_404(employee_id)
        if not 'username' in request.json.keys() or not 'password' in request.json.keys():
            return {'message': 'Debe de capturar username y password'}, 400
        if Employee.username_is_available(request.json['username'], employee_id) > 0:
            return {'message': f"El nombre de usuario {request.json['username']} ya se encuentra en uso"}, 400
        password_hashed = generar_password(request.json['password'])
        employee.save_username_password(request.json['username'], password_hashed)
        employee = Employee.query.get_or_404(employee_id)

        return {'data': employee_schema.dump(employee)}, 200



class CategorieListController(Resource):
    support_cors = True
    cors_origin = '*'
    cors_headers = 'origin, content-type, x-request-with'

    def get(self):
        categories = Category.query.all()
        if not categories:
            return {'data': []}, 404
        
        return {'data': categories_schema.dump(categories)}, 200

    def post(self):
        payload = request.json
        new_category = Category(**payload)
        db.session.add(new_category)
        db.session.commit()
        return {'data': category_schema.dump(new_category)}, 201



class CategoriaController(Resource):
    support_cors = True
    cors_origin = '*'
    cors_headers = 'origin, content-type, x-request-with'

    def get(self, pk):
        categoria = Category.query.get_or_404(pk)
        if not categoria:
            return {"data": None, "message": "No se encontro categoria"}, 404
        return {"data": category_schema.dump(categoria), "message": "Success"}, 200

    def put(self, pk: int):
        payload = request.json
        categoria = Category.query.get_or_404(pk)
        if not categoria:
            return {"data": None, "message": "No se encontro categoria"}, 404
        for key, value in payload.items():
            setattr(categoria, key, value)
        db.session.commit()
        return self.get(pk)
    
    def patch(self, pk: int):
        return self.put(pk)


class InvoiceCustomerListController(Resource):
    support_cors = True
    cors_origin = '*'
    cors_headers = 'origin, content-type, x-request-with'
    def get(self):
        customers = InvoiceCustomer.query.all()
        if not customers:
            return {'data': [], 'message': 'No data'}, 404
        return {'data': customers_schema.dump(customers), 'message': 'success'}, 200

    def post(self):
        payload = request.json
        new_customer = InvoiceCustomer(**payload)
        db.session.add(new_customer)
        db.session.commit()
        return {'data': customer_schema.dump(new_customer)}, 201


class InvoiceCustomerController(Resource):
    support_cors = True
    cors_origin = '*'
    cors_headers = 'origin, content-type, x-request-with'

    def get(self, pk):
        customer = InvoiceCustomer.query.get_or_404(pk)
        if not customer:
            return {"data": None, "message": "No se encontro cliente"}, 404
        return {"data": customer_schema.dump(customer), "message": "Success"}, 200

    def put(self, pk):
        customer = InvoiceCustomer.query.get_or_404(pk)
        if not customer:
            return {"data": None, "message": "No se encontro cliente"}, 404
        payload = request.json
        for key, value in payload.items():
            setattr(customer, key, value)
        db.session.commit()
        return self.get(pk)

    def patch(self, pk):
        return self.put(pk=pk)


class EstadosListController(Resource):
    support_cors = True
    cors_origin = '*'
    cors_headers = 'origin, content-type, x-request-with'

    def get(self):
        return {"data": estados, "message": "success"}, 200


class MunicipiosListronController(Resource):
    support_cors = True
    cors_origin = '*'
    cors_headers = 'origin, content-type, x-request-with'

    def get(self, estado):
        municipios = municipios_por_estado[estado]
        return {'data': municipios, 'message': 'success'}, 200

    
