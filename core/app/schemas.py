from dataclasses import fields
from core.extensions import ma

class EmployeeSchema(ma.Schema):

    class Meta:
        fields = ('id', 'firstName', 'lastName', 'position', 'startDate', 'salary', 'username','password')
    

class CategorySchema(ma.Schema):

    class Meta:
        fields = ('idCategoria', 'icon', 'image', 'name', 'title_list', 'url', 'view', 'state')

category_schema = CategorySchema()
categories_schema = CategorySchema(many=True)

employee_schema = EmployeeSchema()
employees_schema = EmployeeSchema(many=True)


class CustomerSchema(ma.Schema):

    class Meta:
        fields = ('id_cliente_facturacion','id_cliente', 'razon_social', 'rfc', 'calle', 'numero_exterio',
                  'numero_interior', 'tipo_persona','clave_pais', 'estado', 'ciudad',
                  'colonia', 'contacto', 'email_facturacion')


customer_schema = CustomerSchema()
customers_schema = CustomerSchema(many=True)
