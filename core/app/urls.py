from re import I
from flask import Blueprint
from flask_restful import Api


from .resoursces import (
    EmployeeListResource, 
    EmployeeResource,
    EmployeeSavePassword,
    CategorieListController,
    CategoriaController,
    InvoiceCustomerListController,
    InvoiceCustomerController,
    EstadosListController,
    MunicipiosListronController
)

blueprint = Blueprint('urls', __name__)
api = Api(blueprint, prefix='/api/')

api.add_resource(EmployeeListResource, 'employees')
api.add_resource(EmployeeResource, 'employees/<int:id>')
api.add_resource(EmployeeSavePassword, 'employee/<int:employee_id>/credentials')
api.add_resource(CategorieListController, 'categories')
api.add_resource(CategoriaController, "category/<int:pk>")
api.add_resource(InvoiceCustomerListController, 'customers')
api.add_resource(InvoiceCustomerController, 'customer/<int:pk>')
api.add_resource(EstadosListController, 'estados')
api.add_resource(MunicipiosListronController, 'estado/<string:estado>/municipios')
