import datetime
from datetime import datetime, timedelta
from flask import Flask, request, jsonify

from sqlalchemy import func
from flask_cors import CORS, cross_origin
from flask_restful import Api, Resource, request
from  werkzeug.security import generate_password_hash, check_password_hash
import jwt
from .extensions import db, ma
from core.app.models import Employee
from core.app.urls import blueprint
from password import generar_password, check_password


app = Flask(__name__)
cors = CORS(app, resources={r"*": {"origins": "*"}})
app.config['SECRET_KEY']='004f2af45d3a4e161a7dd2d17fdae47f'
#app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///employees.db'
app.config['SQLALCHEMY_DATABASE_URI'] = "mysql+pymysql://root:lmZ1415&@localhost/marketplace"
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

#Inyectamos la instancia de Flask a las extensiones
db.init_app(app=app)
ma.init_app(app)
api = Api(app)

app.register_blueprint(blueprint)

@app.route("/api/login", methods=['POST'])
def login():
    if not 'username' in request.json.keys() or not 'password' in request.json.keys():
        return jsonify(mensaje='Inválido')
    employee = Employee.query.filter(Employee.username == request.json['username']).first()
    if not check_password(request.json['password'], employee.password):
        return jsonify(message='Usuario o contraseña incorrectos')
    token = jwt.encode({
        'public_id': employee.id,
        'exp' : datetime.utcnow() + timedelta(minutes = 30)
    }, app.config['SECRET_KEY'])
    return jsonify(message='Login exitoso', token=token, refreshToken=token)
 

@app.route("/api/token", methods=['POST'])
def token_user():
    return jsonify(message='success')


