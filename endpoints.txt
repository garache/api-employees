
URL	Method	Action
/api/employees	GET	Get list of all Employees
/api/employees	POST	Create new Employee
/api/employees/delete	POST	Delete Employees by ids
/api/employees/{id}	PUT	Update Employee