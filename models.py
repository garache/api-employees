import enum
from sqlalchemy import Enum
from app import db



class Employee(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    firstName = db.Column(db.String(100))
    lastName = db.Column(db.String(100))
    position = db.Column(db.String(100))
    startDate = db.Column(db.Date())
    salary = db.Column(db.Float())

    def __init__(self, firstName, lastName, position, startDate, salary):
        self.firstName = firstName
        self.lastName = lastName
        self.position = position
        self.startDate = startDate



class Position(enum.Enum):
    CEO = 'CEO'
    SOFTWARE_ENGINEER = 'SOFTWARE_ENGINEER'
    SENIOR_JAVASCRIPT_DEVELOPER = 'SOFTWARE_ENGINEER'
    INTEGRATION_SPECIALIST = 'INTEGRATION_SPECIALIST'