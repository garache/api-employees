import bcrypt


def generar_password(text_plain: str) -> str:
    text_enconde = text_plain.encode()
    return bcrypt.hashpw(text_enconde, bcrypt.gensalt())


def check_password(password_text_plain: str, password_hash: bytes) -> bool:
    return bcrypt.checkpw(password_text_plain.encode(), password_hash.encode())