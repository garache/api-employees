from flask_restful import Resource
from models import Business
from schemas import business_schema, businesses_schema

class BusinessListResource(Resource):
    def get(self):
        data = Business.query.all()
        return businesses_schema.dump(data)

    
