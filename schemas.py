from app import ma

class BusinessSchema(ma.Schema):
    class Meta:
        fields = ('id', 'person_name', 'business_name', 'business_gst_number')
    

business_schema = BusinessSchema()
businesses_schema = BusinessSchema(many=True)